from .serializers import SizeCalculatorSerializer
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import ListModelMixin
from calculator.models import SizeCalculator


class SizeCalculatorViewSet(ListModelMixin, GenericViewSet):
    queryset = SizeCalculator.objects.all()
    serializer_class = SizeCalculatorSerializer

    def get_queryset(self):
        queryset = SizeCalculator.objects.all()
        min_file_size = self.request.query_params.get("min")
        max_file_size = self.request.query_params.get("max")
        file_name = self.request.query_params.get("file_name")
        if min_file_size and max_file_size:
            queryset = queryset.filter(file_size__gt=min_file_size, file_size__lt=max_file_size)
        if file_name:
            queryset = queryset.filter(file_name=file_name)
        return queryset
