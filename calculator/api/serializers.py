from rest_framework.serializers import ModelSerializer
from calculator.models import SizeCalculator


class SizeCalculatorSerializer(ModelSerializer):
    class Meta:
        model = SizeCalculator
        fields = ("file", "file_size", "file_name")
