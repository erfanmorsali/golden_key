from rest_framework.routers import DefaultRouter
from .api.views import SizeCalculatorViewSet

router = DefaultRouter()
router.register("files", SizeCalculatorViewSet, "file")

app_name = "calculator"
urlpatterns = []

urlpatterns += router.urls
