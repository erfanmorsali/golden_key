from django.db import models
import os


class SizeCalculator(models.Model):
    file = models.FileField(upload_to="calculator/")
    file_size = models.CharField(max_length=20, null=True, blank=True)
    file_name = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return str(self.file_name)

    def save(self, *args, **kwargs):
        self.file_size = self.file.size
        file_name = os.path.splitext(self.file.name)[0]
        self.file_name = file_name.split("/")[1]
        super().save(*args, **kwargs)
